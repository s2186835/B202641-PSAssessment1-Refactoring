from os import kill
import sys
import numpy as np
sys.path.append('predator_prey')
#from predator_prey import simulate_predator_prey
from simulate_predator_prey import *

def testGetVersion():
    assert getVersion() == 1.0, \
           "wrong version"

def TestHPModel():
    iters = 1
    N = 20
    M = 10
    line = "{} {}".format(M, N)
    assert True == True


def testinit():
    r = 0.08
    a = 0.04
    k = 0.2
    b = 0.02
    m = 0.06
    l = 0.2
    dt = 0.4
    t = 10
    d = 500
    lfile = "map.dat"
    hseed = 1
    pseed = 1
    model = HPModel(r, a, k, b, m, l, dt, t, d, lfile, hseed, pseed)
    assert model.r == r and model.a == a and model.k == k and model.b == b and model.m == m and model.l == l and model.dt == dt and model.t == t and model.d == d and model.hseed == hseed and model.pseed == pseed


def testreadfile():
    r = 0.08
    a = 0.04
    k = 0.2
    b = 0.02
    m = 0.06
    l = 0.2
    dt = 0.4
    t = 10
    d = 500
    lfile = "map.dat"
    hseed = 1
    pseed = 1
    model = HPModel(r, a, k, b, m, l, dt, t, d, lfile, hseed, pseed)
    assert model.readFile() == True, "Readfile failed"

def testInitMatrix():
    r = 0.08
    a = 0.04
    k = 0.2
    b = 0.02
    m = 0.06
    l = 0.2
    dt = 0.4
    t = 10
    d = 500
    lfile = "map.dat"
    hseed = 1
    pseed = 1
    model = HPModel(r, a, k, b, m, l, dt, t, d, lfile, hseed, pseed)
    print(model.hs)
    hs_result = np.array([0, 0.67182122, 4.23716868, 3.81887309, 1.27534513, 2.47717544, 2.24745532, 3.25796486, 3.94361676, 0.46929793, 0.14173738, 0])
    flag = True
    for i in range(12):
        flag = flag and (abs(model.hs[1,i] - hs_result[i]) < 1)
    assert flag == True, "Init Matrix Assert Failed"
    

if __name__ == '__main__' :
    testInitMatrix()