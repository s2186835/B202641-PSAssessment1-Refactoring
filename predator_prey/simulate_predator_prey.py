from argparse import ArgumentParser
import numpy as np
import random
import time
import os

def getVersion():
    return 1.0

def simCommLineIntf():
    par=ArgumentParser()
    par.add_argument("-r","--birth-hares",type=float,default=0.08,help="Birth rate of hares")
    par.add_argument("-a","--death-hares",type=float,default=0.04,help="Rate at which pumas eat hares")
    par.add_argument("-k","--diffusion-hares",type=float,default=0.2,help="Diffusion rate of hares")
    par.add_argument("-b","--birth-pumas",type=float,default=0.02,help="Birth rate of pumas")
    par.add_argument("-m","--death-pumas",type=float,default=0.06,help="Rate at which pumas starve")
    par.add_argument("-l","--diffusion-pumas",type=float,default=0.2,help="Diffusion rate of pumas")
    par.add_argument("-dt","--delta-t",type=float,default=0.4,help="Time step size")
    par.add_argument("-t","--time_step",type=int,default=10,help="Number of time steps at which to output files")
    par.add_argument("-d","--duration",type=int,default=500,help="Time to run the simulation (in timesteps)")
    par.add_argument("-f","--landscape-file",type=str,required=True,
                        help="Input landscape file")
    par.add_argument("-hs","--hare-seed",type=int,default=1,help="Random seed for initialising hare densities")
    par.add_argument("-ps","--puma-seed",type=int,default=1,help="Random seed for initialising puma densities")
    args=par.parse_args()
    model=HPModel(args.birth_hares,args.death_hares,args.diffusion_hares,args.birth_pumas,args.death_pumas,args.diffusion_pumas,args.delta_t,args.time_step,args.duration,args.landscape_file,args.hare_seed,args.puma_seed)
    model.run()

class HPModel:
    def __init__(self, r, a, k, b, m, l, dt, t, d, lfile, hseed, pseed):
        self.r = r
        self.a = a
        self.k = k
        self.b = b
        self.m = m
        self.l = l
        self.dt = dt
        self.t = t
        self.d = d
        self.lfile = lfile
        self.hseed = hseed
        self.pseed = pseed
        self.avg_file = "averages.csv"
        self.output_dir = "output"
        self.readFile()
        self.initMatrix()

    def readFile(self):
        '''
        Readfiles for initialization
        '''
        print("Predator-prey simulation",getVersion())
        assert os.path.exists(self.lfile), "File not exist"
        with open(self.lfile,"r") as f:
            self.w, self.h=[int(i) for i in f.readline().split(" ")]
            assert self.w > 0 and self.h > 0, "File format wrong: Negtive number of lines"
            print("Width: {} Height: {}".format(self.w, self.h))
            self.wh=self.w+2 # Width including halo
            self.hh=self.h+2 # Height including halo
            self.lscape=np.zeros((self.hh, self.wh),int)
            row=1
            for line in f.readlines():
                values=line.split(" ")
                assert len(values) == self.w, "File format wrong: Missing elements in line"
                # Read landscape into array,padding with halo values.
                self.lscape[row]=[0]+[int(i) for i in values]+[0]
                row += 1
            assert row == self.h+1, "File format wrong: Missing lines"
        self.nlands=np.count_nonzero(self.lscape)
        print("Number of land-only squares: {}".format(self.nlands))
        # Pre-calculate number of land neighbours of each land square.
        return True

    def matrixUpdate(self):
        '''
        Update the Matrix in the Simulation
        '''
        for x in range(1,self.h+1):
            for y in range(1,self.w+1):
                if not self.lscape[x,y]:
                    continue
                h_first_derivative = (self.r*self.hs[x,y])-(self.a*self.hs[x,y]*self.ps[x,y])
                h_second_derivative = self.k*((self.hs[x-1,y]+self.hs[x+1,y]+self.hs[x,y-1]+self.hs[x,y+1])-(self.neibs[x,y]*self.hs[x,y]))
                self.hs_nu[x,y]=self.hs[x,y]+self.dt*(h_first_derivative+h_second_derivative)
                if self.hs_nu[x,y]<0:
                    self.hs_nu[x,y]=0
                p_first_derivative = (self.b*self.hs[x,y]*self.ps[x,y])-(self.m*self.ps[x,y])
                p_second_derivative = self.l*((self.ps[x-1,y]+self.ps[x+1,y]+self.ps[x,y-1]+self.ps[x,y+1])-(self.neibs[x,y]*self.ps[x,y]))
                self.ps_nu[x,y]=self.ps[x,y]+self.dt*(p_first_derivative+p_second_derivative)
                if self.ps_nu[x,y]<0:
                    self.ps_nu[x,y]=0

    #def sim(r,a,k,b,m,l,dt,t,d,lfile,hseed,pseed):
    def initMatrix(self):
        '''
        Initialization for 
            neighbour matrix: neibs
            hare matrix:      hs
            puma matrix:      ps
        '''
        self.neibs=np.zeros((self.hh,self.wh),int)
        for x in range(1,self.h+1):
            for y in range(1,self.w+1):
                self.neibs[x,y]=self.lscape[x-1,y] \
                    + self.lscape[x+1,y] \
                    + self.lscape[x,y-1] \
                    + self.lscape[x,y+1]
        self.hs=self.lscape.astype(float).copy()
        self.ps=self.lscape.astype(float).copy()
        random.seed(self.hseed)
        for x in range(1,self.h+1):
            for y in range(1,self.w+1):
                if self.hseed==0:
                    self.hs[x,y]=0
                else:
                    if self.lscape[x,y]:
                        self.hs[x,y]=random.uniform(0,5.0)
                    else:
                        self.hs[x,y]=0
        random.seed(self.pseed)
        for x in range(1,self.h+1):
            for y in range(1,self.w+1):
                if self.pseed==0:
                    self.ps[x,y]=0
                else:
                    if self.lscape[x,y]:
                        self.ps[x,y]=random.uniform(0,5.0)
                    else:
                        self.ps[x,y]=0
        # Create copies of initial maps and arrays for PPM file maps.
        # Reuse these so we don't need to create new arrays going
        # round the simulation loop.

    def formatPrint(self): #, i: int):
        '''
        print the state of the simulation 
        '''
        assert i >= 0, "Invalid i"
        hcols=np.zeros((self.h,self.w),int)
        pcols=np.zeros((self.h,self.w),int)
        mh=np.max(self.hs)
        mp=np.max(self.ps)
        if self.nlands != 0:
            ah=np.sum(self.hs)/self.nlands
            ap=np.sum(self.ps)/self.nlands
        else:
            ah=0
            ap=0
        print("Averages. Timestep: {} Time (s): {} Hares: {} Pumas: {}".format( \
               i, i*self.dt, ah, ap))
        with open(self.avg_file,"a") as f:
            f.write("{},{},{},{}\n".format(i,i*self.dt,ah,ap))
        for x in range(1,self.h+1):
            for y in range(1,self.w+1):
                if self.lscape[x,y]:
                    if mh != 0:
                        hcol=(self.hs[x,y]/mh)*255
                    else:
                        hcol = 0
                    if mp != 0:
                        pcol=(self.ps[x,y]/mp)*255
                    else:
                        pcol = 0
                    hcols[x-1,y-1]=hcol
                    pcols[x-1,y-1]=pcol
        output_file = os.path.join(self.output_dir, "map_{:04d}.ppm")
        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)
        with open(output_file.format(i),"w") as f:
            hdr="P3\n{} {}\n{}\n".format(self.w,self.h,255)
            f.write(hdr)
            for x in range(0,self.h):
                for y in range(0,self.w):
                    if self.lscape[x+1,y+1]:
                        f.write("{} {} {}\n".format(hcols[x,y],pcols[x,y],0))
                    else:
                        f.write("{} {} {}\n".format(0,0,255))

    def swapIter(self):
        '''
        Change the new and old matrix
        '''
        tmp=self.hs
        self.hs=self.hs_nu
        self.hs_nu=tmp
        tmp=self.ps
        self.ps=self.ps_nu
        self.ps_nu=tmp
        
    def run(self):
        '''
        The main process. It simulates the process
        '''
        self.hs_nu=self.hs.copy()
        self.ps_nu=self.ps.copy()
        if self.nlands != 0:
            ah=np.sum(self.hs)/self.nlands
            ap=np.sum(self.ps)/self.nlands
        else:
            ah=0
            ap=0
        print("Averages. Timestep: {} Time (s): {} Hares: {} Pumas: {}".format( \
               0, 0, ah, ap))
        with open("averages.csv","w") as f:
            hdr="Timestep,Time,Hares,Pumas\n"
            f.write(hdr)
        tot_ts = int(self.d / self.dt)
        for i in range(0,tot_ts):
            if not i%self.t:
                self.formatPrint(i)
            self.matrixUpdate()
            # Swap arrays for next iteration.
            self.swapIter()

if __name__ == "__main__":
    simCommLineIntf()
